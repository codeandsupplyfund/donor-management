class Gift < ApplicationRecord
  belongs_to :donor
  scope :cash_donations, -> { where(type: 'CashDonation') }
end
