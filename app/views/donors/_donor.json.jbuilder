json.extract! donor, :id, :name, :email, :mailing_address, :created_at, :updated_at
json.url donor_url(donor, format: :json)
