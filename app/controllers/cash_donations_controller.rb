class CashDonationsController < ApplicationController
  before_action :set_cash_donation, only: [:show, :edit, :update, :destroy]

  # GET /cash_donations
  # GET /cash_donations.json
  def index
    @cash_donations = CashDonation.all
  end

  # GET /cash_donations/1
  # GET /cash_donations/1.json
  def show
  end

  # GET /cash_donations/new
  def new
    @cash_donation = CashDonation.new
  end

  # GET /cash_donations/1/edit
  def edit
  end

  # POST /cash_donations
  # POST /cash_donations.json
  def create
    @cash_donation = CashDonation.new(cash_donation_params)

    respond_to do |format|
      if @cash_donation.save
        format.html { redirect_to @cash_donation, notice: 'Cash donation was successfully created.' }
        format.json { render :show, status: :created, location: @cash_donation }
      else
        format.html { render :new }
        format.json { render json: @cash_donation.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /cash_donations/1
  # PATCH/PUT /cash_donations/1.json
  def update
    respond_to do |format|
      if @cash_donation.update(cash_donation_params)
        format.html { redirect_to @cash_donation, notice: 'Cash donation was successfully updated.' }
        format.json { render :show, status: :ok, location: @cash_donation }
      else
        format.html { render :edit }
        format.json { render json: @cash_donation.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cash_donations/1
  # DELETE /cash_donations/1.json
  def destroy
    @cash_donation.destroy
    respond_to do |format|
      format.html { redirect_to cash_donations_url, notice: 'Cash donation was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cash_donation
      @cash_donation = CashDonation.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def cash_donation_params
      params.require(:cash_donation).permit(:amount_in_cents, :donor_id, :description, :notes, :given_at)
    end
end
