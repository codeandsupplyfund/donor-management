Rails.application.routes.draw do
  resources :cash_donations
  resources :donors
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
