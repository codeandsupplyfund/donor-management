class CreateGifts < ActiveRecord::Migration[6.0]
  def change
    create_table :gifts do |t|
      t.string :type
      t.integer :amount_in_cents
      t.datetime :given_at
      t.string :description
      t.text :notes
      t.references :donor

      t.timestamps
    end
  end
end
